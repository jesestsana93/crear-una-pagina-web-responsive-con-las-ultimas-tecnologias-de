Curso de Udemy: "Crear una Página Web Responsive con las últimas tecnologías de 2020" por Jonathan Lifschitz
Duración: 3,5 horas

Temas: 
+ HTML5 y CSS3
+ Responsive Design
+ Efectos con la propiedad hover
+ Transformaciones de objetos con la propiedad transform
+ Animaciones avanzadas con jQuery
+ Estructurar el HTML para aumentar el Posicionamiento en los Buscadores

Herramientas en línea:
* Para generar un gradiente: https://www.colorzilla.com/gradient-editor/
* Para generar una sombra: https://www.cssmatic.com/box-shadow
* Libreria para el carousel: https://owlcarousel2.github.io/OwlCarousel2/
* Página para crea un mockup en muchos dispositivos subiendo una imagen: https://dimmy.club/
* Servidor local: https://www.mamp.info/en/